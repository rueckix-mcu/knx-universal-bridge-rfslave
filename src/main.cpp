/**
 * @file main.cpp
 * @author rueckix
 * @brief Slave device for my universal KNX bridge. It uses CC1101 on RF-443 MHz to listen for RF events (-> report to master for KNX bus insertion) and it listens for Serial commands from the master (-> translation to RF events)
 * Currently HomeEasy HE-300/841 and Intertechno (new) pulse codes are supported.
 * @version 0.1
 * @date 2021-03-06
 * 
 * @copyright Copyright (c) 2021
 * 
 * 
 *
 * 
 */

#include <Arduino.h>

#include <ELECHOUSE_CC1101_SRC_DRV.h>
#include <SerialSlave.h>
#include <SoftwareSerial.h>

#include <capture.h>
#include <command.h>
#include <decoder.h>
#include <MultiProtocolEncoder.h>



#include "debug.h"

#define GDO2_RX  2
#define GDO0_TX  3
#define SCK   13
#define CSN   10
#define SO    12
#define SI    11

#define SERIAL_TX   4 
#define SERIAL_RX   5

#ifndef SENDER_ADDRESS
#define SENDER_ADDRESS 23571113
#endif

#define HANDLE_HE300
#define HANDLE_HE841
#define HANDLE_INTERTECHNO


// setup of serial connection to master node
SoftwareSerial mySerial = SoftwareSerial(SERIAL_RX, SERIAL_TX);
SerialSlave slave = SerialSlave(mySerial);


// setup of protocol parser
Capture cap = Capture(GDO2_RX);
Decoder dec = Decoder();

MultiProtocolEncoder enc = MultiProtocolEncoder(GDO0_TX);
SERCOMCommand tmp_cmd;


struct HomeEasyEvent
{
    uint32_t address;
    uint32_t unit;
    bool status;
    bool group;
};



#define REPEAT 3

// implementing a ring buffer
#define FIFO_SIZE 10
SERCOMCommand fifo[FIFO_SIZE];
uint8_t fifo_start = 0;
uint8_t fifo_end = 0;
bool fifo_empty = true;


void setup() {
  Serial.begin(9600);

  if (ELECHOUSE_cc1101.getCC1101()){       // Check the CC1101 Spi connection.
  DBGPRINTln(F("Connection OK"));
  }else{
  DBGPRINTln(F("Connection Error"));
  }

  ELECHOUSE_cc1101.setSpiPin(SCK, SO, SI, CSN);
  

  ELECHOUSE_cc1101.Init();            // must be set to initialize the cc1101!


  ELECHOUSE_cc1101.setMHZ(433.92); // Here you can set your basic frequency. The lib calculates the frequency automatically (default = 433.92).The cc1101 can: 300-348 MHZ, 387-464MHZ and 779-928MHZ. Read More info from datasheet.
   
  ELECHOUSE_cc1101.SetRx();     // set Receive on
  
  //pinMode(GDO2_RX, INPUT);
  
  DBGPRINTln(F("Receiver initialized... "));
  
};
  



void setTx(bool tx)
{
  ELECHOUSE_cc1101.Init(); 
  if (tx)
    ELECHOUSE_cc1101.SetTx(); 
  else
    ELECHOUSE_cc1101.SetRx(); 
}



void emitRF(const CommandParameters &rfcmd)
{
    setTx(true);
    enc.Emit(rfcmd, REPEAT);
    setTx(false);
}


void createSendCommand(const CommandParameters &rfcmd, SERCOMCommand &cmd)
{
     /**
     * Valid SEND commands have the following format
     * 
     * (string, address, unit, group)
     * 
     */

    cmd.clear();

    if (rfcmd.status == true)
      cmd.push_back(F("on"));
    else
      cmd.push_back(F("on"));

    String addr;
    addr.concat(rfcmd.address);
    cmd.push_back(addr);

    String unit;
    unit.concat(rfcmd.unit);
    cmd.push_back(unit);

    String group;
    if (rfcmd.group)
      group = F("1");
    else
      group = F("0");
    
    cmd.push_back(group);
}

void pushFiFo(const SERCOMCommand &cmd)
{
  fifo_empty = false;
  fifo[fifo_end] = cmd;
  fifo_end = (fifo_end +1) % FIFO_SIZE;
  
  // check if we wrapped around when the ring buffer was full
  if (fifo_end == fifo_start)
  {
    fifo_start = (fifo_start +1) % FIFO_SIZE;
  }
  
}

bool popFiFo(SERCOMCommand &cmd)
{
  if (fifo_empty)
  {
    return false;
  }
  
  cmd.clear();
  cmd = fifo[fifo_start];
    
  // check if we hit the end already
  if (fifo_start == fifo_end)
  {
    fifo_empty = true;
  }
  else
  {
    fifo_start = (fifo_start +1) % FIFO_SIZE;
  } 

  return true;

}

void onPoll()
{
  tmp_cmd.clear();

  if (!popFiFo(tmp_cmd))
  {
    tmp_cmd.clear(); // send empty response, indicating end of queue
  }
  
  slave.Respond(tmp_cmd);
}



void handleReceivedCommand (const SERCOMCommand &cmd)
{
  
     /**
     * Valid RECEIVE commands have the following format
     * 
     * (string, unit, group)
     * The address is defined in this code. DEFINE SENDER_ADDRESS
     * 
     */

    CommandParameters event;
    
    if (cmd.size() < 1)
      return; // invalid command

    // determine command type
    String c = cmd.at(0);
    c.toLowerCase();

    if (c.equals(F("on")))
    {
        event.status = true;
    }
    else if (c.equals(F("off")))
    {
        event.status = false;
    }
    else if (c.equals(F("poll")))
    {
        onPoll();
        return;
    }

    // we are handling and on-off event

    event.address = SENDER_ADDRESS;
  
    String unit = cmd.at(1);
    unit.toLowerCase();
    event.unit = unit.toInt();

    if (event.unit == 0)
        return; // invalid command
    
    String group = cmd.at(3);
    event.group = group.toInt();

    emitRF(event);
}

/**
 * @brief Try to capture an over-the-air event and queue it for transmission to the master node
 * 
 */
void loopRF()
{

  uint16_t len = cap.CaptureRaw();
  if (len == 0)
    return;

  PulseBuffer pulsebuf;
  cap.GetBuffer(pulsebuf);
  bool ret;
  CodeBuffer codebuf;
  CommandParameters rfcmd;
  tmp_cmd.clear();

#ifdef HANDLE_HE300
  ret = dec.Decode(pulsebuf, Protocol::HE300); 
  if (ret)
  {
    dec.GetBuffer(codebuf);
    if (Command::Parse(Protocol::HE300, codebuf, rfcmd))
    {
      createSendCommand(rfcmd, tmp_cmd);
      pushFiFo(tmp_cmd);
    }
  }
#endif

#ifdef HANDLE_HE841
  tmp_cmd.clear();
  ret = dec.Decode(pulsebuf, Protocol::HE841);
  if (ret)
  {
    dec.GetBuffer(codebuf);
    if (Command::Parse(Protocol::HE841, codebuf, rfcmd))
    {
      createSendCommand(rfcmd, tmp_cmd);
      pushFiFo(tmp_cmd);
    }
  }
#endif

#ifdef HANDLE_INTERTECHNO
  tmp_cmd.clear();
  ret = dec.Decode(pulsebuf, Protocol::INTERTECHNO);
  if (ret)
  {
    dec.GetBuffer(codebuf);
    if (Command::Parse(Protocol::INTERTECHNO, codebuf, rfcmd))
    {
      createSendCommand(rfcmd, tmp_cmd);
      pushFiFo(tmp_cmd);
    }
  }
#endif
}

void loopSerial()
{
  tmp_cmd.clear();
  if (slave.Receive(tmp_cmd))
  {
    handleReceivedCommand(tmp_cmd);
  }
}

void loop()
{ 
  // handle serial commands
  // TODO: rely on interrupt handling in the backgroud
  // master is working with custom timeout, slave is not, that may cause issues with detecting RF events
  // RF events should be rare, might work

  loopSerial();
  
  // handle RF events
  loopRF();
  
}
